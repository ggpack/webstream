# WebStream

Stream your app logs to a WS endpoint to stream them on your browser without connecting to the app host.

This is a tool used during early dev phase when there is no log aggregator (like Datadog) setup yet.


# Usage

``` go

import "io"
import "net/http"
import "os"
import "log"

import "gitlab.com/ggpack/webstream"

func main() {
	log.Print("Starting")
	streamWriterHandler := webstream.NewWriterHandler()

	// Optional: Combine stdout & streamWriter for dual logging
	dualWriter := io.MultiWriter(os.Stdout, streamWriterHandler)

	// Wire the log library into the websocket
	log.SetOutput(dualWriter)

	// 2 endpoints for the websocket client and the webpage itself
	http.Handle("/ws", streamWriterHandler)
	http.HandleFunc("/logs", webstream.UiHandler("/../ws"))

	// Regular API endpoints
	//http.HandleFunc("/api/cats", handleCatsListing)

	log.Printf("Server listening on %v\n", ":8000")
	log.Fatal(http.ListenAndServe(":8000", nil))
}

```

# UI

It is a minialisting page opening a websocket trying to connect to the wsHandler URL.
It upgrades to `wss:` protocol when it detects it is served over https.
This works well with a Golang proxy `httputil.NewSingleHostReverseProxy` and has been tested with a TLS offload between the UI and the logging backend.

It automatically supports light 🔆 & dark 🌙 mode based on the browser setting `@media (prefers-color-scheme: dark)`.


# Credits
https://betterprogramming.pub/streaming-log-files-in-real-time-with-golang-and-websockets-a-tail-f-simulation-89e080bebfe

# Improvements
Since the events come only one-way from the server, we could try Server-Sent-Events instead of websockets.

# Local testing

``` bash
cd example
go run .
```

In a browser open 2 tabs, one on `http://localhost:8000/logs` and one on `http://localhost:8000`, repeat the requests on `http://localhost:8000` to observe new logs added to the `/logs` tab.
