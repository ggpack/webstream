package webstream

import (
	"net/http"
)

// wsPath is the extra piece of path relative to the path the page itself is served on.
// This way it is agnostic to the location it is served (proxy, ingress, https)
func UiHandler(wsPath string) http.HandlerFunc {

	return func(res http.ResponseWriter, req *http.Request) {
		allTemplates.ExecuteTemplate(res, "index.html", map[string]string{"WsPath": wsPath})
	}
}
