package webstream

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// One tab = one client
type Client struct {
	socket *websocket.Conn
	send   chan []byte
}

// Central point managing the clients connections & broadcasting the messages to the clients
type WriterHandler struct {
	broadcast  chan string
	clients    map[*Client]bool // As many as open tabs
	register   chan *Client
	unregister chan *Client
}

// Handlesnew messages to send to the clients
func (this *WriterHandler) Write(message []byte) (n int, err error) {
	this.broadcast <- string(message)
	return len(message), nil
}

// Handles new websockets connections
func (this *WriterHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := &Client{ws, make(chan []byte)}
	this.register <- client

	go func() {
		defer func() {
			this.unregister <- client
			ws.Close()
		}()

		for {
			_, _, err := ws.ReadMessage()
			if err != nil {
				this.unregister <- client
				ws.Close()
				break
			}
		}
	}()

	go func() {
		defer ws.Close()
		for {
			message, ok := <-client.send
			if !ok {
				ws.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			ws.WriteMessage(websocket.TextMessage, message)
		}
	}()
}

func NewWriterHandler() *WriterHandler {
	this := &WriterHandler{
		broadcast:  make(chan string),
		clients:    make(map[*Client]bool),
		register:   make(chan *Client),
		unregister: make(chan *Client),
	}

	go func() {
		for {
			select {
			case client := <-this.register:
				this.clients[client] = true
			case client := <-this.unregister:
				if _, ok := this.clients[client]; ok {
					delete(this.clients, client)
					close(client.send)
				}
			case message := <-this.broadcast:
				for client := range this.clients {
					client.send <- []byte(message)
				}
			}
		}
	}()

	return this
}
