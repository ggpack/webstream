module example

go 1.22

require gitlab.com/ggpack/webstream v0.0.0

require github.com/gorilla/websocket v1.5.3 // indirect

replace gitlab.com/ggpack/webstream v0.0.0 => ../
