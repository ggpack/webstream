package main

import (
	"io"
	"log"
	"net/http"
	"os"

	"gitlab.com/ggpack/webstream"
)

func main() {
	log.Print("Starting")
	streamWriterHandler := webstream.NewWriterHandler()

	// Optional: Combine stdout & streamWriter for dual logging
	dualWriter := io.MultiWriter(os.Stdout, streamWriterHandler)

	// Wire the log library into the websocket
	log.SetOutput(dualWriter)

	// 2 endpoints for the websocket client and the webpage itself
	http.Handle("/ws", streamWriterHandler)
	http.HandleFunc("/logs", webstream.UiHandler("/../ws"))

	// Regular API endpoints
	http.HandleFunc("/", handleListing)

	log.Printf("Server listening on %v\n", ":8000")
	log.Fatal(http.ListenAndServe(":8000", nil))
}

func handleListing(res http.ResponseWriter, req *http.Request) {
	log.Print("Listing")
	res.Write([]byte("La liste 🐈 🐕"))
}
