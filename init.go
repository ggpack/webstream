package webstream

import (
	"embed"
	"html/template"
	"log"
)

//go:embed templates
var allFS embed.FS

var allTemplates *template.Template

// Good ref https://www.convictional.com/blog/go-embed
func init() {
	allTemplates = template.Must(template.New("").ParseFS(allFS, "templates/*"))
	log.Printf("The template loaded: %s", allTemplates.DefinedTemplates())
}
